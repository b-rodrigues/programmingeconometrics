get_sd <- function(y, exog, theta, ddf){
    Standard.Error <- 1/sqrt(diag(ddf(theta, y, exog)))
    return(Standard.Error)
}

descent <-function(y, exog, df, ddf, init, step = 0.1, eps=1e-10, maxiter=20, hist=F)
{
    theta <- init
    i <- 1
    hist_theta <- matrix(0, maxiter, length(theta))
    #are_equal(dim(cbind(theta)), c(1,3))
    continue <- TRUE
    while(continue==TRUE) {
        theta.old <- theta
        hist_theta[i, ] <- cbind(theta.old)
        i <- i+1
        theta <- theta - step * df(theta, y, exog)
        continue <- (abs(theta-theta.old) > eps) && (i <= maxiter)
    }
    if (i > maxiter) { 
        warning("Maximum number of iterations reached: gradient descent may not have converged")
        Estimate <- theta
        Standard.Error <- get_sd(y, exog, theta, ddf)
        
        if (hist == F){
            return(list("Estimation results"=as.data.frame(cbind(Estimate, Standard.Error))))
        } else {
            return(list("Trace"=hist_theta,
                        "Estimation results"=list("Coefficients"=Estimate, "Standard Error"=Standard.Error)))
        }
        
    } else {
        Estimate <- theta
        Standard.Error <- get_sd(y, exog, theta, ddf)
        if (hist == F){
            return(list("Estimation results"=as.data.frame(cbind(Estimate, Standard.Error))))
        } else {
            return(list("Trace"=hist_theta,
                        "Estimation results"=list("Coefficients"=Estimate, "Standard Error"=Standard.Error)))
        }
    }
}



