# We load numDeriv to compare the result from its grad 
# function to a naive implementation of numerical
# derivatives

library(numDeriv)

n <- 16
h <- 10
x <- 1

# Definition of some function
my_f <- function(x) tan(x)^5

sim <- function(n, h, f, x){
  result <- array(0, dim = c(n, 6))
  for(i in 1:n){ d = h^(-i)
    g1 <- (f(x + d) - f(x))/d
    g2 <- (f(x) - f(x - d))/d
    g3 <- (f(x + d) - f(x - d))/(2*d)
    crit = abs(g1-g3)+abs(g3-g2)+abs(g2-g1)
    result[i, ] <- cbind(grad(f, x), g1, g2, g3, crit, i)
  }
  return(result)
}

# We can see that when h**(-i) gets to close to 0, 
# everything breaks down

result <- sim(n, h, my_f, x)
print(result, digits=15)

# True value of the derivative
plot(seq(1,n), result[,1], type="l", col = 1,  lwd = 2, ylim = c(min(result), max(result)))

# Naive implementation
for (i in 2:4) lines(seq(1,n), result[,i], type="l", col = i, lwd = 2)

print(result[which.min(result[,5]),],digits=7)
