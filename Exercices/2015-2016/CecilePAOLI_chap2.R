# Exercice 1
(a<-c(6,3,8,9))
(b<-c(9,1,3,5))
# Addition de a et b
(c<-(a+b))
# Soustraction de a et b
(d<-(a-b))

# Exercice 2
(a*b)
# Lorsque l'on fait a*b, on obtient [1] 54  3 24 45
# on observe que le logiciel calcule simplement la mutiplication entre a et b, mais on n'obtient pas un scalaire
# Pour calculer le produit scalaire, il faut calculer : (a%*%b) , on obtient 126
(a%*%b)

# Exercice 3
# Nommons A et B les deux matrices demand�es
(A<-matrix(2,nrow=30,ncol=30))
(B<-matrix(4,nrow=30,ncol=30))
(A*B)
# En faisant le produit A*B on obtient simplement la multiplication de chacun des termes de la matrice A avec ceux de la matrice B
# on obtient donc une matrice de dimension (30,30) comprenant que des 8 (� savoir 2*4 pour chaque terme)
(A%*%B)
# En faisant le produit scalaire, on obtient la multiplication des deux matrices.
# on obtient une matrice dont le terme en colonne 1 et ligne 1 par exemple correspond � la somme des A(1j) par B(i1) avec j et i allant de 1 � 30
# � savoir une matrice de dimension (30,30) comprenant que des 240

# Exercice 4
(a<-"cecile")
(b<-"paoli")
(paste(a,b))
# Cette commande permet de "coller" les deux mots les uns � c�t� des autres, dans l'ordre a puis b
# on obtient [1] "cecile paoli"

# Exercice 5
(a<-8)
(b<-3)
(c<-19)
(a>b)
# Cette commande indique "TRUE" car effectivement a est sup�rieur � b car 8>3
(a==b)
# Cette commande indique "FALSE" car a et b ne sont pas �gaux
(a!=b)
# Cette commande indique "TRUE" car a et b sont effectivement diff�rents
(a<b)
# Cette commande indique "FALSE" car a n'est pas inf�rieur � b car 8>3
((a>b)&&(a<c))
# Cette commande indique "TRUE" car on a bien � la fois a qui est sup�rieur � b (car 8>3) et � la fois a qui est inf�rieur � c (car8<19)
((a>b)&&(a>c))
# Cette commande indique "FALSE" car il n'y a qu'une affirmation qui est vraie, � savoir a sup�rieur � b. En revanche, a n'est pas sup�rieur � c
# ces deux in�galit�s ne sont donc pas v�rifi�es en m�me temps
((a>b)||(a<b))
# Cette commande indique "TRUE" car on a : a sup�rieur � b OU a inf�rieur � b. 
# on a bien l'un o� l'autre vu que a est bien sup�rieur � b

# Exercice 6
(A<-matrix(c(9,4,12,5,0,7,2,6,8,9,2,9),nrow=4,byrow=TRUE))
# Question 1
(A>=5)
# Cette commande indique "TRUE" pour tous les termes de la matrice qui sont sup�rieurs � 5 et "FALSE" pour ceux qui sont inf�rieurs � 5
# Question 2
(A[,2])
# Cette commande sort le vecteur de la 2�me colonne de la matrice
# on obtient donc : [1] 4 0 6 2
# Question 3
(t(A))
# Cette commande nous donne la transpos�e de la matrice A

# Exercice 7
# Cr�ons la matrice A
(A<-matrix(c(9,4,12,5,0,7,2,6,8,9,2,9,2,9,0,11),nrow=4,byrow=TRUE))
# Cr�ons un vecteur colonne B = (7,18,1,0)
(B<-rbind(7,18,1,0))
# Pour r�soudre ce syst�me et trouver les valeurs du vecteur (x,y,z,t), il faut calculer A^(-1) * B
(invA<-solve(A))
# Ceci nous donne l'inverse de la matrice A
((invA)%*%B)
# Ceci nous donne le vecteur X