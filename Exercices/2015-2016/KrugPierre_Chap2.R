# Exercices pour le 12/02/2016

# Exercice 1

# cr�ation du vecteur a
a<-cbind(6,3,8,9)
# creation du vecteur b
b<-cbind(9,1,3,5)
# addition des deux vecteurs
c<-a+b
# soustraction des deux vecteurs
d<-a-b


# Exercice 2

# si on fait a*b
a*b
# on obtient le produit d'un scalaire avec son scalaire correspondant dans l'autre matrice
# Pour faire le produit scalaire sur R on utlise la commande "%*%" 
# si les matrices sont conformes
a%*%t(b)


# Exercice 3

# creation de la matrice A
A <- matrix(2,30,30)
# creation de la matrice B
B <- matrix(4,30,30)
# multiplication
A*B
# on obtient un matrice 30*30 avec que des 8 
# produit scalaire
A%*%t(B)
# on obtient une matrice 30*30 avec que des 240
# les deux op�rations ne donnent pas de scalaires mais a chaque fois une matrice 30*30


# Exercice 4

a<-"pierre"
b<-"krug"
paste(a,b)
# cette commande rasssemble deux variables en une seule.


# Exercice 5

a <- 8
b <- 3
c <- 19.

a>b
# TRUE car 8 superieur � 3
a==b
# FALSE car 8 different de 3
a != b
# TRUE car 8 different de 3
a < b
# FALSE car 8 n'est pas inferieur a 3
(a > b) && (a < c)
# TRUE car les deux conditions sont respectees
(a > b) && (a > c)
# FALSE car la deuxi�me conditions n'est pas respectee
(a > b) || (a < b)
# TRUE car une des deux conditions est respectee (la 2eme) 


# Exercice 6

# creation de la matrice A
A<-matrix(c(9,4,12,5,0,7,2,6,8,9,2,9),4,3,TRUE)
A>=5 # effectue la verification >=5 sur chaque coefficient de la matrice
A[,2] # sort le vecteur de la 2eme colonne
t(A) # t() est la commande qui donnne la transpos� de la matrice


# Exercice 7

# creation de la matrice A
A<-matrix(c(9,4,12,2,5,0,7,9,2,6,8,0,9,2,9,11),4,4,TRUE)
# on utilise la fonction "solve" pour inverser la matrice
InverseA<-solve(A)
# creation de la matrice B
B<-rbind(7,18,1,0)
# resolution du systeme
InverseA%*%B

#          [,1]
# [1,] -4.2028571
# [2,] -6.2285714
# [3,]  5.8471429
# [4,] -0.2128571