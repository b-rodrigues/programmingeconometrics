quentin.gollentz@outlook.com

$platform
[1] "i386-w64-mingw32"

$arch
[1] "i386"

$os
[1] "mingw32"

$system
[1] "i386, mingw32"

$status
[1] ""

$major
[1] "3"

$minor
[1] "2.3"

$year
[1] "2015"

$month
[1] "12"

$day
[1] "10"

$`svn rev`
[1] "69752"

$language
[1] "R"

$version.string
[1] "R version 3.2.3 (2015-12-10)"

$nickname
[1] "Wooden Christmas-Tree"
